using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace ZAD3_4
{
    class ProxyLogger : IDataset
    {
        private string filePath;
        private Dataset dataset;
        public ProxyLogger(string filePath)
        {
            this.filePath = filePath;
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            ConsoleLogger.GetInstance().Log("Reading from file");
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }

            return dataset.GetData();
        }
    }
}
