using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace ZAD3_4
{
    class DataConsolePrinter
    {
        public void PrintDataset(IDataset data)
        {
            ReadOnlyCollection<List<string>> stringlist = data.GetData();
            if(stringlist == null)
            {
                Console.WriteLine("Dataset is null");
                return;
            }
            for(int i = 0; i < stringlist.Count; i++)
            {
                for(int j = 0; j < stringlist[i].Count; j++)
                {
                    Console.Write(stringlist[i][j] + "\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
