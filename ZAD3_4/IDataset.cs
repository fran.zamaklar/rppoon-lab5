using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace ZAD3_4
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();

    }
}
