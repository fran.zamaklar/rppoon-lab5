using System;

namespace ZAD3_4
{
    class Entry
    {
        static void Main(string[] args)
        {
            //ZAD 3

            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset(@"C:\Users\Lenovo\Documents\sensitiveData.csv");
            User firstuser = User.GenerateUser("Izabela");
            User seconduser = User.GenerateUser("Marko");
            ProtectionProxyDataset protectionProxy1 = new ProtectionProxyDataset(firstuser);
            ProtectionProxyDataset protectionProxy2 = new ProtectionProxyDataset(seconduser);
            DataConsolePrinter printer = new DataConsolePrinter();

            printer.PrintDataset(virtualProxyDataset);
            printer.PrintDataset(protectionProxy1);
            printer.PrintDataset(protectionProxy2);

            //ZAD 4

            ProxyLogger proxyLogger = new ProxyLogger(@"C:\Users\Lenovo\Documents\sensitiveData.csv");
            printer.PrintDataset(proxyLogger);
        }
    }
}
