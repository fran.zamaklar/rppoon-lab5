using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD5_6_7
{
    class RedTheme : ITheme
    {
        public string GetFooter(int width)
        {
            return new string("---------------------");
        }

        public string GetHeader(int width)
        {
            return new string("---------------------");
        }

        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.Red;
        }

        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
