using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD5_6_7
{
    class GroupNote : Note
    {
        private List<string> users;
        public GroupNote(string message, ITheme theme) : base(message, theme) 
        {
            users = new List<string>();
        }

        public void Add(string user)
        {
            users.Add(user);
        }

        public void Remove(string user)
        {
            users.Remove(user);
        }

        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("GROUP REMINDER: ");
            string framedMessage = this.GetFramedMessage();
            Console.WriteLine(framedMessage);
            for(int i = 0; i < users.Count; i++)
            {
                Console.WriteLine(users[i]);
            }
            Console.WriteLine();
            Console.ResetColor();
        }
    }
}
