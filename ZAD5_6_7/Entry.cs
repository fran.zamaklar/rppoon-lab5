using System;
using System.Text.RegularExpressions;

namespace ZAD5_6_7
{
    class Entry
    {
        static void Main(string[] args)
        {
            //ZAD 5 

            ReminderNote reminderNote = new ReminderNote("Crvena je najljepsa", new RedTheme());
            reminderNote.Show();
            Console.WriteLine();

            //ZAD 6 

            GroupNote semifinal1 = new GroupNote("Eurovizija - polufinale 1", new LightTheme());
            semifinal1.Add("Hrvatska");
            semifinal1.Add("Izreal");
            semifinal1.Add("Finska");
            GroupNote semifinal2 = new GroupNote("Eurovizija - polufinale 2", new RedTheme());
            semifinal2.Add("Belgija");
            semifinal2.Add("Austrija");
            semifinal2.Add("Armenija");
            semifinal1.Show();
            semifinal2.Show();

            //ZAD 7

            Notebook notes = new Notebook();
            notes.AddNote(reminderNote);
            notes.AddNote(semifinal1);
            notes.AddNote(semifinal2);
            notes.Display();
            notes.ChangeTheme(new RedTheme());
            notes.Display();

            Notebook secondnotes = new Notebook(new LightTheme());
            secondnotes.AddNoteTheme(reminderNote);
            secondnotes.AddNoteTheme(semifinal1);
            secondnotes.AddNoteTheme(semifinal2);
            secondnotes.Display();
        }
    }
}
