using System;
using System.Collections.Generic;

namespace ZAD1_2
{
    class Entry
    {
        static void Main(string[] args)
        {
            //1 ZAD
            Product product = new Product("Autici", 100, 4);
            Product product1 = new Product("Lutke", 50, 1);
            Box toybox = new Box("Kutijica");
            toybox.Add(product);
            toybox.Add(product1);
            Console.WriteLine(toybox.Weight);
            Console.WriteLine(toybox.Price);

            //2 ZAD
            ShippingService shippingService = new ShippingService(2);
            Console.WriteLine(shippingService.GetDeliveryPrice(toybox));
        }
    }
}
