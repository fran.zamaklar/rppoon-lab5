using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD1_2
{
    class ShippingService
    {
        public double pricePerKilogram;

        public ShippingService(double price)
        {
            pricePerKilogram = price;
        }

        public double GetDeliveryPrice(IShipable box)
        {
            double price = box.Weight * pricePerKilogram;
            return price;
        }
    }
}
